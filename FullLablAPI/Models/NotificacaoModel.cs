﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FullLablAPI.Models
{
    public class NotificacaoModel
    {
        public string Titulo { get; set; }
        public string Mensagem { get; set; }
    }
}