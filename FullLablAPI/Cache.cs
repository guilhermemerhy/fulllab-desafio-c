﻿using FullLablAPI.Redis;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FullLablAPI
{
    public class Cache
    {
        RedisConnection redis = new RedisConnection();

        public Cache()
        {
        }

        public void CountVisitas(long idProduto, string loja)
        {
            var data = DateTime.Now.Date;
            IDatabase db = redis.Connection.GetDatabase();

            var chave = $"{loja}_{data.ToShortDateString()}_{idProduto}";
            var valor = (int)db.StringGet(chave);

            valor = valor + 1;
            db.StringSet(chave, valor);
            db.KeyPersist(chave);

        }

        public int TotalVisitasProduto(long idProduto, DateTime data, string loja)
        {
            IDatabase db = redis.Connection.GetDatabase();

            var chave = $"{loja}_{data.ToShortDateString()}_{idProduto}";
            return (int)db.StringGet(chave);


        }
    }
}