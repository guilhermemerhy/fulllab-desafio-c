﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace FullLablAPI.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/search")]
    public class SearchController : BaseController
    {
        const string Taco = "taco";
        const string Polishop = "polishop";

        [HttpGet]
        [Route("vtexpolishop/{pesquisa}")]
        public Task<HttpResponseMessage> VtexPolishop(string pesquisa)
        {
            using (WebClient wc = new WebClient { Encoding = Encoding.UTF8 })
            {
                try
                {
                    var json = wc.DownloadString($"http://polishop.vtexcommercestable.com.br/api/catalog_system/pub/products/search/{pesquisa}");

                    return CreateResponse(HttpStatusCode.OK, json);
                }
                catch (Exception ex)
                {
                    return CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
        }


        [HttpGet]
        [Route("vtextaco/{pesquisa}")]
        public Task<HttpResponseMessage> VtexTaco(string pesquisa)
        {
            using (WebClient wc = new WebClient { Encoding = Encoding.UTF8 })
            {
                try
                {
                    var json = wc.DownloadString($"http://taco.vtexcommercestable.com.br/api/catalog_system/pub/products/search/{pesquisa}");

                    return CreateResponse(HttpStatusCode.OK, json);
                }
                catch (Exception ex)
                {
                    return CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
        }

        [HttpGet]
        [Route("findtaco/{id}")]
        public Task<HttpResponseMessage> FindTaco(long id)
        {
            using (WebClient wc = new WebClient { Encoding = Encoding.UTF8 })
            {
                try
                {
                    new Cache().CountVisitas(id, Taco);

                    var json = wc.DownloadString($"http://taco.vtexcommercestable.com.br/api/catalog_system/pub/products/search?fq=productId:{id}");

                    return CreateResponse(HttpStatusCode.OK, json);
                }
                catch (Exception ex)
                {
                    return CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
        }

        [HttpGet]
        [Route("findpolishop/{id}")]
        public Task<HttpResponseMessage> FindPolishp(long id)
        {
            using (WebClient wc = new WebClient { Encoding = Encoding.UTF8 })
            {
                try
                {
                    new Cache().CountVisitas(id, Polishop);

                    var json = wc.DownloadString($"http://polishop.vtexcommercestable.com.br/api/catalog_system/pub/products/search?fq=productId:{id}");


                    return CreateResponse(HttpStatusCode.OK, json);
                }
                catch (Exception ex)
                {
                    return CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
        }

        [HttpGet]
        [Route("totalVisitasPolishop")]
        public Task<HttpResponseMessage> TotalVisitas(long id, DateTime data)
        {
            try
            {
                return CreateResponse(HttpStatusCode.OK, new Cache().TotalVisitasProduto(id, data, Polishop));
            }
            catch (Exception)
            {
                return CreateResponse(HttpStatusCode.BadRequest, "");
            }
        }

        [HttpGet]
        [Route("totalVisitasTaco")]
        public Task<HttpResponseMessage> TotalVisitasTaco(long id, DateTime data)
        {

            try
            {
                 return CreateResponse(HttpStatusCode.OK, new Cache().TotalVisitasProduto(id, data, Taco));
            }
            catch (Exception)
            {

                return CreateResponse(HttpStatusCode.BadRequest, "");
            }           
        }
    }
}